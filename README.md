## Requirements

python3
(Tested using python 3.6)

## Running

Can be run either as it is using a hard coded string representation of the game as provided in the PDF

`python bowling.py`

This will use the game string: `23432/XX428/X218-`

Alternatively it can be run with any valid representation of a game string, for example:

`python bowling.py "XXXXXXXXX8/5"`

or

`python bowling.py "XXXXXXXXXXXX"`

or

`python bowling.py "34539/X54X-5725/XX7"`

Any string can be provided for the game string as desired, and the code will validate it conforms to a valid string representation of a bowling game