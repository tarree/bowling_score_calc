from sys import argv

class GameStringFormatException(Exception):
    pass


def get_game_frames(game):
    # A bowling game is made of 10 frames, with either 1, 2 or 3 "balls" in each frame.
    # If we therefore split the string into it's frames and balls, we can more easily calculate the score.
    # Using this method we can also validate the provided string to ensure it is a valid string
    # representation of a game
    frames = []
    current_frame = None
    complete_frame = False
    for char in game:
        try:
            the_char = int(char)
            if not 0 < the_char < 10:
                raise GameStringFormatException("Found an invalid integer value in the game string")
        except ValueError:
            if char in ["X", "/", "-"]:
                the_char = char
            else:
                raise GameStringFormatException("Unexpected character found in the game string")
        # Check if the current frame is None
        # if it is we need to setup for a new frame,
        # this happens if this is the first character in the game string, or if the previous iteration of the
        # loop was the end of a frame
        if current_frame is None:
            current_frame = {
                "balls": [],
                "score": 0
            }

        # Append the current ball to the current frame
        current_frame["balls"].append(the_char)

        # As we are parsing the entire string here first to validate it is accurate, we can also grab the base score
        # for the frame here as well.  Strikes and spares have a base score of 10, while any simple integers have
        # a base score of their value added to the total for the frame
        if the_char in ["X", "/"]:
            current_frame["score"] = 10
        elif type(the_char) is int:
            current_frame["score"] += the_char

        if len(frames) == 9:
            # if the total number of frames is 9, this is the final frame, so we can have up to 3 balls in this frame,
            # this frame can also have up to 3 strikes, or a spare and a strike, or a spare and a single ball
            if len(current_frame["balls"]) == 3:
                complete_frame = True
            elif len(current_frame["balls"]) == 2:
                # If the length of the final frame is two, this can mean one of several things:

                # If both balls were integers, that is the end of the frame, and the end of the game
                # or if the second ball was an open ball then that is again the end of the frame and the game
                if (type(current_frame["balls"][0]) is int and type(current_frame["balls"][1]) is int) \
                        or current_frame["balls"][1] == "-":
                    complete_frame = True
                elif the_char in ["/", "X"] or (current_frame["balls"][0] and type(the_char) is int):
                    # if none of the above is true,
                    # then we should just double check that this ball is a strike or a spare
                    # This also handles a case where you get a strike in the first ball of the final frame,
                    # but then only some pins on the second through, you can still have a bonus through
                    # because of the strike
                    pass
                else:
                    raise GameStringFormatException("Unexpected value for the final frame")

        elif len(frames) < 9:
            # If it's not the final frame, we need to do some checks to see if we have enough balls to
            # complete the frame, this includes spares, strikes, or two simple balls

            if the_char == "X":
                # If this ball was a strike, we can end the frame assuming it is the only ball in the frame,
                # if it is not the only ball then the game string provided was invalid, so raise an error
                if len(current_frame["balls"]) == 1:
                    complete_frame = True
                else:
                    raise GameStringFormatException("Invalid frame provided")
            elif the_char == "/":
                # if we have a spare here, we should see the balls for the current frame be 2 long, and the first
                # ball being an integer, if this is true, the frame can be completed.  If this is not the case,
                # then there is an error in the string provided and we should raise an error
                if len(current_frame["balls"]) == 2 and type(current_frame["balls"][0]) is int:
                    complete_frame = True
                else:
                    raise GameStringFormatException("Invalid frame provided")
            elif char == "-":
                # if the ball was an open ball and it was the second ball in the frame, that's the end of the frame
                # assuming that the first ball was also an open ball, or was an integer.
                # If it was the first ball in the frame, we can safely carry on through the game
                # If anything else happens here, we need to raise an exception
                if len(current_frame["balls"]) == 2 and (type(current_frame["balls"][0]) is int or the_char == "-"):
                    complete_frame = True
                elif len(current_frame["balls"]) == 1:
                    pass
                else:
                    raise GameStringFormatException("Invalid frame provided")
            else:

                if len(current_frame["balls"]) == 2:
                    complete_frame = True
        else:
            raise GameStringFormatException("Too many frames found for the game")

        if complete_frame:
            # If the frame has been marked as complete, we can add it to the list of frames
            # We should then reset the current_frame and the complete_frame flag ready for the next frame
            frames.append(current_frame)
            current_frame = None
            complete_frame = False

    return frames


def get_next_balls_score(next_balls, index):
    try:
        # Simply grabs the next ball score based on the next available balls and the index provided
        nb = next_balls[index]
        if nb == "X":
            return 10
        elif nb == "/":
            return 10 - next_balls[index-1]
        elif nb == "-":
            return 0
        else:
            return nb
    except IndexError:
        return 0


def calculate_score(frames):
    total_score = 0
    # Enumerate through all the frames that we have parsed from the previous step
    for i, frame in enumerate(frames):
        # For strikes and spares we will need the next balls thrown by the player,
        # So we might as well get them here, and make a list for easy access later on
        next_balls = []
        try:
            next_balls.extend(frames[i+1]["balls"])
        except IndexError:
            pass
        try:
            next_balls.extend(frames[i+2]["balls"])
        except IndexError:
            pass

        if frame["balls"][0] == "X":
            # If the first ball of this frame is a strike, we need to get the score of the next two balls thrown
            # by the player to count as the score for this frame
            frame_score = frame["score"] + get_next_balls_score(next_balls, 0) + get_next_balls_score(next_balls, 1)
        elif frame["balls"][1] == "/":
            # If the second ball thrown in this frame resulted in a spare we need to grab the next ball thrown
            # by the player to get the score for this frame
            frame_score = frame["score"] + get_next_balls_score(next_balls, 0)
        else:
            # If we simply got two "numbers" for the pins knocked down on this frame, or a handful of pins,
            # followed by an open ball or vice versa then we can simply take the base score for this frame which
            # is calculated as the number of pins knocked down
            frame_score = frame["score"]

        if i == 9:
            # On the final frame, a few special cases can happen as you can potentially have an extra bonus
            # throw based on the result of the first two balls of the frame
            if len(frame["balls"]) == 3:
                # If the first ball of the final frame was a strike, we can add an additional 10 points to the
                # total based on the above calculation of the score
                if frame["balls"][0] == "X":
                    frame_score += 10
                # if the second ball is a strike or a spare and not an integer, then we can add an additional
                # 10 points here as well, as again the rest of the score is handled by the above
                if frame["balls"][1] in ["X", "/"] and not type(frame["balls"][2]) is int:
                    frame_score += 10

        # Once we have the actual frame score for this frame, we can add it to the total
        total_score += frame_score

        # I've added in this here to update the frame score so it keeps a running total of the score for the game
        # partially for verification of the code working as intended, and not just ending up with a
        # lucky guess of the final outcome of the score
        frame["score"] = total_score

    return total_score


def main():
    # Just using sys.argv so I can potentially pass in a random string of characters
    # so I can verify it's not just luck producing the right answer.
    # Could have better validation here to ensure the string passed in as the first argument (after the filename)
    # is a valid game string, but that's beyond the scope of the task for now
    if len(argv) > 1:
        game_string = argv[1]
    else:
        # if we have no argument passed in for the game string, use the one provided from the PDF
        game_string = "23432/XX428/X218-"

    print("Calculating score for the following string: {0}".format(game_string))

    try:
        frames = get_game_frames(game_string)
    except GameStringFormatException as exc:
        print("There appears to be an error in the format of the game string provided: {0}".format(str(exc)))
        return 1

    # Calculate the game score given the provided game string, either user entered or hard coded based on the above
    print("Score: {0}".format(calculate_score(frames)))


if __name__ == "__main__":
    main()
